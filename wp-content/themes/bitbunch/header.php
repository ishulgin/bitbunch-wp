<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<?php $page_slug = basename(get_permalink()); ?>
	<div id="mobile-menu" class="mobile-menu">
		<div class="mobile-menu__head">
			<a href="/"><img src="<?php echo get_template_directory_uri(); ?>/images/logo-light.svg" alt="Bitbunch"></a>
			<a id="hide_mobile_menu" class="hide_mobile_menu" href="javascript:void(0)"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/close.svg" /></a>
		</div>
		<div class="mobile-menu__navi">
			<ul>
				<li>
					<a href="/how-it-works">How it works</a>
				</li>

				<li>
					<a href="/pressroom">Pressroom</a>
				</li>

				<li>
					<a href="/team">Team</a>
				</li>

				<li>
					<a href="/faq">FAQ</a>
				</li>

				<li>
					<a href="/career">Career</a>
				</li>

				<li>
					<a href="/contacts">Contact</a>
				</li>
			</ul>
		</div>
		<div class="mobile-menu__auth-navi">
			<a class="btn btn-signin" href="#">sign in</a>
			<a class="btn btn-register" href="#">register</a>
		</div>
	</div>
	<header class="site-header">
		<div class="site-header__internal">
			<div class="site-header__split">
				<div class="site-header__segment site-header__segment-logo">
					<a href="/">
						<img src="<?php echo get_template_directory_uri(); ?>/images/logo-light.svg" alt="Bitbunch">
					</a>
				</div>
				<div class="site-header__segment site-header__segment-navi">
					<ul>
						<li>
							<a href="/how-it-works">How it works</a>
						</li>

						<li>
							<a href="/pressroom">Pressroom</a>
						</li>

						<li>
							<a href="/team">Team</a>
						</li>

						<li>
							<a href="/faq">FAQ</a>
						</li>

						<li>
							<a href="/career">Career</a>
						</li>

						<li>
							<a href="/contacts">Contact</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="site-header__split">
				<div class="site-header__segment site-header__segment-mobile">
					<ul>
						<li>
							<a href="javascript:void(0)" id="mobile_navi_blob" class="mobile_navi_blob">
								<img src="<?php echo get_template_directory_uri(); ?>/images/icons/hamburger.svg" />
							</a>
						</li>
						<li>
							<a href="/" class="btn orange_btn">Register</a>
						</li>
					</ul>
				</div>
				<div class="site-header__segment site-header__segment-auth">
					<ul>
						<li>
							<a href="/">Sign In</a>
						</li>

						<li>
							<a href="/" class="btn orange_btn">Register</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</header>
	<div class="site-hero">
		<?php
			switch($page_slug) {
				case 'how-it-works': get_template_part( 'template-parts/hero-content/how-it-works' ); break;
				case 'pressroom': get_template_part( 'template-parts/hero-content/pressroom' ); break;
				case 'contact-us': get_template_part( 'template-parts/hero-content/contact-us' ); break;
				case 'career': get_template_part( 'template-parts/hero-content/career' ); break;
				case 'faq': get_template_part( 'template-parts/hero-content/faq' ); break;
				case 'login': get_template_part( 'template-parts/hero-content/login' ); break;
				case 'register': get_template_part( 'template-parts/hero-content/register' ); break;
				default: get_template_part( 'template-parts/hero-content/homepage' );
			}
		?>
	</div>
	<div id="content" class="site-content">
