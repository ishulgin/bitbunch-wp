$(document).ready(function() {
    $("#mobile_navi_blob").click(function() {
        $("#mobile-menu").addClass('active');
    });
    $("#hide_mobile_menu").click(function() {
        $("#mobile-menu").removeClass('active');
    });
    
    /*
        How it works sliders
    */
    var investment_amount_slider_element = document.getElementById('investment_amount_slider');
    console.log(investment_amount_slider_element);
    if (investment_amount_slider_element) {
        var investment_amount_slider_instance = noUiSlider.create(investment_amount_slider_element, {
            start: 27,
            range: {
                min: 0,
                max: 100
            },
            step: 1,
            connect: [true, false]
        });
        investment_amount_slider_instance.on('update', function(values) {
            var value = parseInt(values[0]);
            document.getElementById('investment_amount_slider_value').innerHTML = value;
        });
    }
    var investment_period_slider_element = document.getElementById('investment_period_slider');
    if (investment_period_slider_element) {
        var investment_period_slider_instance = noUiSlider.create(investment_period_slider_element, {
            start: 12,
            range: {
                min: 1,
                max: 24
            },
            step: 1,
            connect: [true, false]
        });
        investment_period_slider_instance.on('update', function(values) {
            var value = parseInt(values[0]);
            document.getElementById('investment_period_slider_value').innerHTML = value;
        });
    }
});

function openCareerBlock(id) {
    if ($('#' + id).hasClass('active')) {
        $('#' + id).removeClass('active');
    } else {
        $('#' + id).addClass('active');
    }
}

function openFaqItem(id) {
    if ($('#' + id).hasClass('active')) {
        $('#' + id).removeClass('active');
    } else {
        $('#' + id).addClass('active');
    }
}