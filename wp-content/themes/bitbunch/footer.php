<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

?>

	</div>

	<?php
			$current_page = sanitize_post( $GLOBALS['wp_the_query']->get_queried_object() );
			$slug = $current_page->post_name;
	?>

	<footer class="footer-main <?php echo $slug; ?>">
		<div class="footer-main__inside">
				<?php if ($slug === 'homepage') { ?>
				<div class="footer-main__earn">
						<div class="container">
                <div class="row">
                    <div class="col">
                        <div class="footer-main__earn__title">
                            <p>One more thing</p>
                            <p>We earn only if you earn</p>
                        </div>
                        <div class="footer-main__earn__sub">
														<p>
																<span>$</span>
																<span>O</span>
														</p>
														<p>
																Base monthly price
														</p>
                        </div>
												<div class="footer-main__earn__ad">
														<div class="ad_box">
																<div class="ad_box__left">
																		<p>
																				We want to be fair with you
																		</p>
																		<p>
																				Our pricing is pretty simple<br />
																				we charge only <span><span>30</span>%</span> from your profit.
																		</p>
																		<p>
																				<a href="#">Learn more</a>
																		</p>
																</div>
																<div class="ad_box__right">
																		<a class="btn" href="#">Let's earn together</a>
																</div>
														</div>
												</div>
                    </div>
                </div>
            </div>
				</div>
				<?php } ?>
				<div class="footer-main__links">
					<div class="container">
						<div class="row footer-main__links__row">
							<div class="col-6 col-md-3 mb-3 mb-md-0 footer-main__links__row__list">
								<ul>
									<li>Support</li>
									<li><a href="mailto:hello@bitbunch.co" class="enlarge_link">hello@bitbunch.co</a></li>
									<li><a href="tel:+447911257986" class="enlarge_link">+44 7911 257986</a></li>
									<li>
										<div class="white-box">
											Mo-Fr <span>9-18</span>
										</div>
									</li>
								</ul>
							</div>

							<div class="col-6 col-md-3 mb-3 mb-md-0 footer-main__links__row__list">
								<ul class="text-md-right">
									<li>Our product</li>
									<li><a href="/how-it-works">How it works</a></li>
									<li><a href="#">About us</a></li>
									<li><a href="#">Support</a></li>
									<li><a href="/faq">F.A.Q.</a></li>
								</ul>
							</div>

							<div class="col-6 col-md-3 footer-main__links__row__list">
								<ul class="text-md-right">
									<li>Company</li>
									<li><a href="/pressroom">Pressroom</a></li>
									<li><a href="/contacts">Contact</a></li>
									<li><a href="/career">Career</a></li>
									<li><a href="/team">Team</a></li>
								</ul>
							</div>

							<div class="col-6 col-md-3 footer-main__links__row__list">
								<ul class="text-md-right">
									<li>Legal</li>
									<li><a href="#">Terms &amp; Conditions</a></li>
									<li><a href="#">Privacy Policy</a></li>
									<li><a href="#">Imprint</a></li>
									<li><a href="#">GDPR</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<div class="footer-main__copy">
					<div class="container">
						<div class="row align-items-center py-4">
							<div class="col-md text-center text-md-left font-size-12 font-weight-bold opacity-70 order-2 order-md-1 my-4 my-md-0 footer-main__copy__copyright">
								© 2020 Bitbunch | All rights reserved
							</div>

							<div class="col-md text-center order-3 order-md-2">
								<img src="<?php echo get_template_directory_uri(); ?>/images/logo-icon.svg" alt="BitBunch">
							</div>

							<div class="col-md text-center text-md-right order-1 order-md-3 footer-main__copy__social">
								<ul class="list-inline mb-0">
									<li class="list-inline-item">
										<a href="#" class="d-inline-block footer-main__copy__social__icon">
											<img src="<?php echo get_template_directory_uri(); ?>/images/icons/google-plus-white.svg" alt="Google+">
										</a>
									</li>

									<li class="list-inline-item">
										<a href="#" class="d-inline-block footer-main__copy__social__icon">
											<img src="<?php echo get_template_directory_uri(); ?>/images/icons/instagram-white.svg" alt="Instagram">
										</a>
									</li>

									<li class="list-inline-item">
										<a href="#" class="d-inline-block footer-main__copy__social__icon">
											<img src="<?php echo get_template_directory_uri(); ?>/images/icons/facebook-white.svg" alt="Facebook">
										</a>
									</li>

									<li class="list-inline-item">
										<a href="#" class="d-inline-block footer-main__copy__social__icon">
											<img src="<?php echo get_template_directory_uri(); ?>/images/icons/linkedin-white.svg" alt="LinkedIn"/>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
		</div>
		<div class="footer-main__disc">
				<div class="container footer-main__disc__text text-center font-weight-bold p-3 px-md-5">
					Investors should be aware that system response, execution price, speed, liquidity, market data, and account
					access times are affected by many factors, including market volatility, size and type of order, market
					conditions, system performance, and other factors.
				</div>
		</div>
		<?php get_template_part( 'template-parts/footer/footer', 'widgets' ); ?>
	</footer>

</div>

<?php wp_footer(); ?>

</body>
</html>
