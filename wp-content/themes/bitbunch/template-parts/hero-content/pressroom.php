<div class="site-hero__root">
    <img src="<?php echo get_template_directory_uri(); ?>/images/bg/jmb-2.jpg" srcset="<?php echo get_template_directory_uri(); ?>/images/bg/jmb-2.jpg 1x, <?php echo get_template_directory_uri(); ?>/images/bg/jmb-2@2x.jpg 2x" alt="Bitbunch" class="site-hero__root__overlay" />
    <div class="pressroom_hero">
        <img class="desktop" src="<?php echo get_template_directory_uri(); ?>/images/bg/pressroom_hero.png" />
        <img class="tablet" src="<?php echo get_template_directory_uri(); ?>/images/bg/pressroom_hero_tablet.png" />
    </div>
    <div class="site-hero__root__content">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class="title">Pressroom</p>
                    <p class="desc">
                        Welcome to the Bitbunch pressroom. If you wish to<br />
                        make a press inquiry please contact us below.
                    </p>
                    <div class="cta">
                        <a href="#" class="btn btn-lg">contact us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>