<div class="site-hero__root homepage">
    <img src="<?php echo get_template_directory_uri(); ?>/images/bg/jmb-2.jpg" srcset="<?php echo get_template_directory_uri(); ?>/images/bg/jmb-2.jpg 1x, <?php echo get_template_directory_uri(); ?>/images/bg/jmb-2@2x.jpg 2x" alt="Bitbunch" class="site-hero__root__overlay" />
    <div class="site-hero__root__content">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="homepage_hero">
                        <div class="homepage_hero__left">
                            <p class="title">Automated <br />Crypto Trading</p>
                            <p class="desc">
                                Start earning secure profits<br />
                                through Artificial Intelligence.
                            </p>
                            <div class="cta">
                                <a href="#" class="btn btn-lg">register now</a>
                            </div>
                        </div>
                        <div class="homepage_hero__right">
                            <p class="homepage_hero__right__title">Automated Crypto Trading</p>
                            <div class="homepage_hero__right__video">
                                <div class="video_cover">
                                    <img class="video_cover__desktop-img" src="<?php echo get_template_directory_uri(); ?>/images/home/video_cover_desktop.png" />
                                    <img class="video_cover__desktop-mobile" src="<?php echo get_template_directory_uri(); ?>/images/home/video_cover_mobile_tablet.png" />
                                </div>
                            </div>
                            <p class="homepage_hero__right__desc">Start earning secure profits through Artificial Intelligence.</p>
                            <div class="homepage_hero__right__cta">
                                <a href="#" class="btn">register now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>