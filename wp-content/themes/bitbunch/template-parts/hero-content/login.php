<div class="site-hero__root tiny">
    <img src="<?php echo get_template_directory_uri(); ?>/images/bg/auth_hero.png" alt="Bitbunch" class="site-hero__root__overlay" />
    <div class="site-hero__root__content">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class="title">Sign In</p>
                    <p class="desc">Welcome back</p>
                </div>
            </div>
        </div>
    </div>
</div>