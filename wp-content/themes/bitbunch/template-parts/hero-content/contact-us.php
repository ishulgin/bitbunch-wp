<div class="site-hero__root">
    <img src="<?php echo get_template_directory_uri(); ?>/images/bg/jmb-2.jpg" srcset="<?php echo get_template_directory_uri(); ?>/images/bg/jmb-2.jpg 1x, <?php echo get_template_directory_uri(); ?>/images/bg/jmb-2@2x.jpg 2x" alt="Bitbunch" class="site-hero__root__overlay" />
    <div class="site-hero__root__content">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="form_based_hero">
                        <div class="form_based_hero__left">
                            <p class="title">Contact us</p>
                            <p class="desc">
                                Feel free to get in touch and let us know<br />
                                how we can help you.
                            </p>
                        </div>
                        <div class="form_based_hero__right">
                            <form class="contact_us_form">
                                <div class="contact_us_form__el">
                                    <input type="text" class="form-control" placeholder="Your email" />
                                </div>
                                <div class="contact_us_form__el">
                                    <input type="text" class="form-control" placeholder="Your name" />
                                </div>
                                <div class="contact_us_form__el">
                                    <textarea class="form-control textarea" placeholder="Your message"></textarea>
                                </div>
                                <div class="contact_us_form__el">
                                    <button class="btn" onclick="return false;">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>