<div class="site-hero__root">
    <img src="<?php echo get_template_directory_uri(); ?>/images/bg/jmb-2.jpg" srcset="<?php echo get_template_directory_uri(); ?>/images/bg/jmb-2.jpg 1x, <?php echo get_template_directory_uri(); ?>/images/bg/jmb-2@2x.jpg 2x" alt="Bitbunch" class="site-hero__root__overlay" />
    <div class="site-hero__root__content site-hero__root__content--how-it-works">
        <div class="hero_image">
            <img src="<?php echo get_template_directory_uri(); ?>/images/bg/how_it_works_hero_image.png" />
        </div>
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class="title">Explore<br />How it works</p>
                    <p class="desc">
                        Our mission is to establish a secure, transparent and<br />
                        profitable alternative to classical investment.
                    </p>
                    <div class="cta">
                        <a href="#" class="btn btn-lg">get started</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>