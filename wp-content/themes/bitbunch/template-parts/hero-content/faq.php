<div class="site-hero__root">
    <img src="<?php echo get_template_directory_uri(); ?>/images/bg/jmb-2.jpg" srcset="<?php echo get_template_directory_uri(); ?>/images/bg/jmb-2.jpg 1x, <?php echo get_template_directory_uri(); ?>/images/bg/jmb-2@2x.jpg 2x" alt="Bitbunch" class="site-hero__root__overlay" />
    <div class="site-hero__root__content">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class="title">Help Center</p>
                    <p class="desc">
                        Find in our FAQ’s fast and correct answers<br />
                        on your questions
                    </p>
                    <div class="cta">
                        <a href="#" class="btn btn-lg">contact support</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>