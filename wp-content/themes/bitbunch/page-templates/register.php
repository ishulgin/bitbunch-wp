<?php /* Template Name: Register */ get_header(); ?>

<div class="auth_page register_page">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="login_page__form auth_form">
                    <div class="auth_form__head">
                        <p>Register your account.</p>
                        <p>It’s 100% free. Forever.</p>
                    </div>
                    <div class="auth_form__body">
                        <form>
                            <div class="auth_form__body__row">
                                <div class="auth_form__body__row__el">
                                    <input type="text" class="form-control" placeholder="Username" />
                                </div>
                            </div>
                            <div class="auth_form__body__row">
                                <div class="auth_form__body__row__el">
                                    <input type="text" class="form-control" placeholder="E-mail" />
                                </div>
                            </div>
                            <div class="auth_form__body__row">
                                <div class="auth_form__body__row__el reset_margin_bottom">
                                    <input type="password" class="form-control" placeholder="Password" />
                                </div>
                            </div>
                            <div class="auth_form__body__row flex">
                                <div class="auth_form__body__row__el">
                                    <div class="checkbox">
                                        <input type="checkbox" id="remember_me" name="remember_me" />
                                        <label for="remember_me">
                                            <span>I read and agree to the <a href="#">Terms and Conditions</a> of use</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="auth_form__body__row">
                                <div class="auth_form__body__row__el">
                                    <button class='btn'>register</button>
                                </div>
                            </div>
                            <div class="auth_form__body__row">
                                <div class="auth_form__body__row__tran">
                                    <p>Already have an account? <a href="#">Sign In</a></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="login_page__form auth_disc">
                    <p>
                      <img src="<?php echo get_template_directory_uri(); ?>/images/icons/auth_security.png" />
                    </p>
                    <p>
                      We take your privacy and data security very seriously at Bitbunch.
                      Bitbunch uses the same 256-bit encryption and physical security used 
                      by banks. Our practices are monitored and verified by TrustArc.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();