<?php /* Template Name: Faq */ get_header(); ?>

<div class="faq">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="faq__inside">
                    <div class="faq__inside__left">
                        <div class="faq__inside__left__wrap-txt">
                            <p class="faq__inside__left__txt">
                                Didn't find an answer to your question?
                            </p>
                            <p class="faq__inside__left__txt">
                                Our technical support team is always ready to help you.
                            </p>
                        </div>
                        <div class="faq__inside__left__txt-block-wrap">
                            <div class="faq__inside__left__txt-block">
                                <div class="faq__inside__left__txt-block__head">
                                    <p>Contact us any time at</p>
                                </div>
                                <div class="faq__inside__left__txt-block__block">
                                    <div class="icon">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/faq/mail_icon.png" />
                                    </div>
                                    <a class="txt" href="mailto:help@bitbunch.com">help@bitbunch.com</a>
                                </div>
                            </div>
                            <div class="faq__inside__left__txt-block">
                                <div class="faq__inside__left__txt-block__head">
                                    <p>Use our support line</p>
                                    <p>Mo-Fr 9-18</p>
                                </div>
                                <div class="faq__inside__left__txt-block__block">
                                    <div class="icon">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/faq/phone_icon.png" />
                                    </div>
                                    <a class="txt" href="+447911257986">+ 44 7911 257986</a>
                                </div>
                            </div>
                        </div>
                        <div class="faq__inside__left__also">
                            <p>You can also</p>
                            <a href="#" class="btn">create a new ticket</a>
                        </div>
                    </div>
                    <div class="faq__inside__right">
                        <div class="questions">
                            
                            <div class="questions__segment">
                                <p class="questions__head">general questions</p>
                                <div class="questions__q">
                                    <div class="questions__q__item" id="questions__q__item_1">
                                        <div class="questions__q__item__head" onclick="openFaqItem('questions__q__item_1')">
                                            <p>What you can earn with Bitbunch</p>
                                            <a href="javascript:void(0)">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/faq/plus_icon.png" />
                                            </a>
                                        </div>
                                        <div class="questions__q__item__body">
                                            <p>
                                                The process consists of a completing an online questionnaire and
                                                submitting supporting documentation. A signed declaration confirming
                                                the accuracy of the submitted information is required, authorising
                                                Bitbunch to complete the verification process.
                                            </p>
                                            <p>
                                                Processing times for verification can take up to 1-2 days, depending on
                                                the level of public interest. 
                                            </p>
                                        </div>
                                    </div>
                                    <div class="questions__q__item" id="questions__q__item_2">
                                        <div class="questions__q__item__head" onclick="openFaqItem('questions__q__item_2')">
                                            <p>How much does a Bitbunch cost</p>
                                            <a href="javascript:void(0)">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/faq/plus_icon.png" />
                                            </a>
                                        </div>
                                        <div class="questions__q__item__body">
                                            <p>
                                                The process consists of a completing an online questionnaire and
                                                submitting supporting documentation. A signed declaration confirming
                                                the accuracy of the submitted information is required, authorising
                                                Bitbunch to complete the verification process.
                                            </p>
                                            <p>
                                                Processing times for verification can take up to 1-2 days, depending on
                                                the level of public interest. 
                                            </p>
                                        </div>
                                    </div>
                                    <div class="questions__q__item" id="questions__q__item_3">
                                        <div class="questions__q__item__head" onclick="openFaqItem('questions__q__item_3')">
                                            <p>Supported exchanges/coins</p>
                                            <a href="javascript:void(0)">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/faq/plus_icon.png" />
                                            </a>
                                        </div>
                                        <div class="questions__q__item__body">
                                            <p>
                                                The process consists of a completing an online questionnaire and
                                                submitting supporting documentation. A signed declaration confirming
                                                the accuracy of the submitted information is required, authorising
                                                Bitbunch to complete the verification process.
                                            </p>
                                            <p>
                                                Processing times for verification can take up to 1-2 days, depending on
                                                the level of public interest. 
                                            </p>
                                        </div>
                                    </div>
                                    <div class="questions__q__item" id="questions__q__item_4">
                                        <div class="questions__q__item__head" onclick="openFaqItem('questions__q__item_4')">
                                            <p>Locked account</p>
                                            <a href="javascript:void(0)">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/faq/plus_icon.png" />
                                            </a>
                                        </div>
                                        <div class="questions__q__item__body">
                                            <p>
                                                The process consists of a completing an online questionnaire and
                                                submitting supporting documentation. A signed declaration confirming
                                                the accuracy of the submitted information is required, authorising
                                                Bitbunch to complete the verification process.
                                            </p>
                                            <p>
                                                Processing times for verification can take up to 1-2 days, depending on
                                                the level of public interest. 
                                            </p>
                                        </div>
                                    </div>
                                    <div class="questions__q__item" id="questions__q__item_5">
                                        <div class="questions__q__item__head" onclick="openFaqItem('questions__q__item_5')">
                                            <p>Two factor authentication (2FA)</p>
                                            <a href="javascript:void(0)">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/faq/plus_icon.png" />
                                            </a>
                                        </div>
                                        <div class="questions__q__item__body">
                                            <p>
                                                The process consists of a completing an online questionnaire and
                                                submitting supporting documentation. A signed declaration confirming
                                                the accuracy of the submitted information is required, authorising
                                                Bitbunch to complete the verification process.
                                            </p>
                                            <p>
                                                Processing times for verification can take up to 1-2 days, depending on
                                                the level of public interest. 
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="questions__segment">
                                <p class="questions__head">general questions</p>
                                <div class="questions__q">
                                    <div class="questions__q__item" id="questions__q__item_6">
                                        <div class="questions__q__item__head" onclick="openFaqItem('questions__q__item_6')">
                                            <p>Verification levels</p>
                                            <a href="javascript:void(0)">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/faq/plus_icon.png" />
                                            </a>
                                        </div>
                                        <div class="questions__q__item__body">
                                            <p>
                                                The process consists of a completing an online questionnaire and
                                                submitting supporting documentation. A signed declaration confirming
                                                the accuracy of the submitted information is required, authorising
                                                Bitbunch to complete the verification process.
                                            </p>
                                            <p>
                                                Processing times for verification can take up to 1-2 days, depending on
                                                the level of public interest. 
                                            </p>
                                        </div>
                                    </div>
                                    <div class="questions__q__item" id="questions__q__item_7">
                                        <div class="questions__q__item__head" onclick="openFaqItem('questions__q__item_7')">
                                            <p>How to get verified</p>
                                            <a href="javascript:void(0)">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/faq/plus_icon.png" />
                                            </a>
                                        </div>
                                        <div class="questions__q__item__body">
                                            <p>
                                                The process consists of a completing an online questionnaire and
                                                submitting supporting documentation. A signed declaration confirming
                                                the accuracy of the submitted information is required, authorising
                                                Bitbunch to complete the verification process.
                                            </p>
                                            <p>
                                                Processing times for verification can take up to 1-2 days, depending on
                                                the level of public interest. 
                                            </p>
                                        </div>
                                    </div>
                                    <div class="questions__q__item" id="questions__q__item_8">
                                        <div class="questions__q__item__head" onclick="openFaqItem('questions__q__item_8')">
                                            <p>Linked bank accounts</p>
                                            <a href="javascript:void(0)">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/faq/plus_icon.png" />
                                            </a>
                                        </div>
                                        <div class="questions__q__item__body">
                                            <p>
                                                The process consists of a completing an online questionnaire and
                                                submitting supporting documentation. A signed declaration confirming
                                                the accuracy of the submitted information is required, authorising
                                                Bitbunch to complete the verification process.
                                            </p>
                                            <p>
                                                Processing times for verification can take up to 1-2 days, depending on
                                                the level of public interest. 
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="questions__segment">
                                <p class="questions__head">Withdraw/Deposit</p>
                                <div class="questions__q">
                                    <div class="questions__q__item" id="questions__q__item_9">
                                        <div class="questions__q__item__head" onclick="openFaqItem('questions__q__item_9')">
                                            <p>Supported methods</p>
                                            <a href="javascript:void(0)">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/faq/plus_icon.png" />
                                            </a>
                                        </div>
                                        <div class="questions__q__item__body">
                                            <p>
                                                The process consists of a completing an online questionnaire and
                                                submitting supporting documentation. A signed declaration confirming
                                                the accuracy of the submitted information is required, authorising
                                                Bitbunch to complete the verification process.
                                            </p>
                                            <p>
                                                Processing times for verification can take up to 1-2 days, depending on
                                                the level of public interest. 
                                            </p>
                                        </div>
                                    </div>
                                    <div class="questions__q__item" id="questions__q__item_10">
                                        <div class="questions__q__item__head" onclick="openFaqItem('questions__q__item_10')">
                                            <p>Deposited wrong coins</p>
                                            <a href="javascript:void(0)">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/faq/plus_icon.png" />
                                            </a>
                                        </div>
                                        <div class="questions__q__item__body">
                                            <p>
                                                The process consists of a completing an online questionnaire and
                                                submitting supporting documentation. A signed declaration confirming
                                                the accuracy of the submitted information is required, authorising
                                                Bitbunch to complete the verification process.
                                            </p>
                                            <p>
                                                Processing times for verification can take up to 1-2 days, depending on
                                                the level of public interest. 
                                            </p>
                                        </div>
                                    </div>
                                    <div class="questions__q__item" id="questions__q__item_11">
                                        <div class="questions__q__item__head" onclick="openFaqItem('questions__q__item_11')">
                                            <p>Missing withdrawals</p>
                                            <a href="javascript:void(0)">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/faq/plus_icon.png" />
                                            </a>
                                        </div>
                                        <div class="questions__q__item__body">
                                            <p>
                                                The process consists of a completing an online questionnaire and
                                                submitting supporting documentation. A signed declaration confirming
                                                the accuracy of the submitted information is required, authorising
                                                Bitbunch to complete the verification process.
                                            </p>
                                            <p>
                                                Processing times for verification can take up to 1-2 days, depending on
                                                the level of public interest. 
                                            </p>
                                        </div>
                                    </div>
                                    <div class="questions__q__item" id="questions__q__item_12">
                                        <div class="questions__q__item__head" onclick="openFaqItem('questions__q__item_12')">
                                            <p>Deposit does not arrive</p>
                                            <a href="javascript:void(0)">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/faq/plus_icon.png" />
                                            </a>
                                        </div>
                                        <div class="questions__q__item__body">
                                            <p>
                                                The process consists of a completing an online questionnaire and
                                                submitting supporting documentation. A signed declaration confirming
                                                the accuracy of the submitted information is required, authorising
                                                Bitbunch to complete the verification process.
                                            </p>
                                            <p>
                                                Processing times for verification can take up to 1-2 days, depending on
                                                the level of public interest. 
                                            </p>
                                        </div>
                                    </div>
                                    <div class="questions__q__item" id="questions__q__item_13">
                                        <div class="questions__q__item__head" onclick="openFaqItem('questions__q__item_13')">
                                            <p>Withdraw to wrong address</p>
                                            <a href="javascript:void(0)">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/faq/plus_icon.png" />
                                            </a>
                                        </div>
                                        <div class="questions__q__item__body">
                                            <p>
                                                The process consists of a completing an online questionnaire and
                                                submitting supporting documentation. A signed declaration confirming
                                                the accuracy of the submitted information is required, authorising
                                                Bitbunch to complete the verification process.
                                            </p>
                                            <p>
                                                Processing times for verification can take up to 1-2 days, depending on
                                                the level of public interest. 
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();