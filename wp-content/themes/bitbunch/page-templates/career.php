<?php /* Template Name: Career */ get_header(); ?>

<div class="career">
    <div class="career__list">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="career__list__disc">
                        <p>Currently we have 8 open job offers</p>
                    </div>
                    <div class="career__list__item" id="career__list__item_1">
                        <div class="career__list__item__head" onclick="openCareerBlock('career__list__item_1')">
                            <div class="lih__left">
                                <div class="lih__left__icon">
                                </div>
                                <div class="lih__left__txt">
                                    <p>development</p>
                                    <p>Lead Frontend Developer</p>
                                    <p>London, UK</p>
                                </div>
                            </div>
                            <div class="lih__right">
                                <p>London, UK</p>
                                <p><a class="btn" href="javascript:void(0)">Show detail</a></p>
                            </div>
                        </div>
                        <div class="career__list__item__info">
                            <div class="career__list__item__info__txt">
                                <p>
                                    <span>About us:</span><br />
                                    We are developing a smart software that helps people investing into cryptocurrencies without the usual risk and without any prior knowledge. Generally
                                    speaking, our software does three different things. It analyses, calculates, acts and learns. It does this for thousands of different trade opportunities per
                                    minute, millions time per day.
                                </p>
                                <p>
                                    <span>About you:</span><br />
                                    Candidate should have excellent verbal, and written, communication skills; have the ability to work independently; and use good time management practices.
                                </p>
                                <p>
                                    <span>Qualifications:</span><br />
                                    - Strong experience retrieving information from blockchains, and wallets;
                                    - Great Java skills; reactive experience is the plus
                                    - Strog Spring 5, and Spring Boot 2, skills
                                </p>
                                <p>
                                    <span>Education:</span><br />
                                    Bachelor's degree or higher in computer science
                                </p>
                            </div>
                            <div class="career__list__item__info__cta">
                                <a href="#" class="btn">apply for this position</a>
                            </div>
                        </div>
                    </div>

                    <div class="career__list__item" id="career__list__item_2">
                        <div class="career__list__item__head" onclick="openCareerBlock('career__list__item_2')">
                            <div class="lih__left">
                                <div class="lih__left__icon">
                                </div>
                                <div class="lih__left__txt">
                                    <p>customer support</p>
                                    <p>Sr. Technical Support Manager</p>
                                    <p>Remote</p>
                                </div>
                            </div>
                            <div class="lih__right">
                                <p>Remote</p>
                                <p><a class="btn" href="javascript:void(0)">Show detail</a></p>
                            </div>
                        </div>
                        <div class="career__list__item__info">
                            <div class="career__list__item__info__txt">
                                <p>
                                    <span>About us:</span><br />
                                    We are developing a smart software that helps people investing into cryptocurrencies without the usual risk and without any prior knowledge. Generally
                                    speaking, our software does three different things. It analyses, calculates, acts and learns. It does this for thousands of different trade opportunities per
                                    minute, millions time per day.
                                </p>
                                <p>
                                    <span>About you:</span><br />
                                    Candidate should have excellent verbal, and written, communication skills; have the ability to work independently; and use good time management practices.
                                </p>
                                <p>
                                    <span>Qualifications:</span><br />
                                    - Strong experience retrieving information from blockchains, and wallets;
                                    - Great Java skills; reactive experience is the plus
                                    - Strog Spring 5, and Spring Boot 2, skills
                                </p>
                                <p>
                                    <span>Education:</span><br />
                                    Bachelor's degree or higher in computer science
                                </p>
                            </div>
                            <div class="career__list__item__info__cta">
                                <a href="#" class="btn">apply for this position</a>
                            </div>
                        </div>
                    </div>

                    <div class="career__list__item" id="career__list__item_3">
                        <div class="career__list__item__head" onclick="openCareerBlock('career__list__item_3')">
                            <div class="lih__left">
                                <div class="lih__left__icon">
                                </div>
                                <div class="lih__left__txt">
                                    <p>sales department</p>
                                    <p>Key Account Manager</p>
                                    <p>London, UK</p>
                                </div>
                            </div>
                            <div class="lih__right">
                                <p>London, UK</p>
                                <p><a class="btn" href="javascript:void(0)">Show detail</a></p>
                            </div>
                        </div>
                        <div class="career__list__item__info">
                            <div class="career__list__item__info__txt">
                                <p>
                                    <span>About us:</span><br />
                                    We are developing a smart software that helps people investing into cryptocurrencies without the usual risk and without any prior knowledge. Generally
                                    speaking, our software does three different things. It analyses, calculates, acts and learns. It does this for thousands of different trade opportunities per
                                    minute, millions time per day.
                                </p>
                                <p>
                                    <span>About you:</span><br />
                                    Candidate should have excellent verbal, and written, communication skills; have the ability to work independently; and use good time management practices.
                                </p>
                                <p>
                                    <span>Qualifications:</span><br />
                                    - Strong experience retrieving information from blockchains, and wallets;
                                    - Great Java skills; reactive experience is the plus
                                    - Strog Spring 5, and Spring Boot 2, skills
                                </p>
                                <p>
                                    <span>Education:</span><br />
                                    Bachelor's degree or higher in computer science
                                </p>
                            </div>
                            <div class="career__list__item__info__cta">
                                <a href="#" class="btn">apply for this position</a>
                            </div>
                        </div>
                    </div>

                    <div class="career__list__item" id="career__list__item_4">
                        <div class="career__list__item__head" onclick="openCareerBlock('career__list__item_4')">
                            <div class="lih__left">
                                <div class="lih__left__icon">
                                </div>
                                <div class="lih__left__txt">
                                    <p>design</p>
                                    <p>Product Design Lead</p>
                                    <p>UK or Remote</p>
                                </div>
                            </div>
                            <div class="lih__right">
                                <p>UK or Remote</p>
                                <p><a class="btn" href="javascript:void(0)">Show detail</a></p>
                            </div>
                        </div>
                        <div class="career__list__item__info">
                            <div class="career__list__item__info__txt">
                                <p>
                                    <span>About us:</span><br />
                                    We are developing a smart software that helps people investing into cryptocurrencies without the usual risk and without any prior knowledge. Generally
                                    speaking, our software does three different things. It analyses, calculates, acts and learns. It does this for thousands of different trade opportunities per
                                    minute, millions time per day.
                                </p>
                                <p>
                                    <span>About you:</span><br />
                                    Candidate should have excellent verbal, and written, communication skills; have the ability to work independently; and use good time management practices.
                                </p>
                                <p>
                                    <span>Qualifications:</span><br />
                                    - Strong experience retrieving information from blockchains, and wallets;
                                    - Great Java skills; reactive experience is the plus
                                    - Strog Spring 5, and Spring Boot 2, skills
                                </p>
                                <p>
                                    <span>Education:</span><br />
                                    Bachelor's degree or higher in computer science
                                </p>
                            </div>
                            <div class="career__list__item__info__cta">
                                <a href="#" class="btn">apply for this position</a>
                            </div>
                        </div>
                    </div>

                    <div class="career__list__item" id="career__list__item_5">
                        <div class="career__list__item__head" onclick="openCareerBlock('career__list__item_5')">
                            <div class="lih__left">
                                <div class="lih__left__icon">
                                </div>
                                <div class="lih__left__txt">
                                    <p>customer support</p>
                                    <p>Support Engineer</p>
                                    <p>Remote</p>
                                </div>
                            </div>
                            <div class="lih__right">
                                <p>Remote</p>
                                <p><a class="btn" href="javascript:void(0)">Show detail</a></p>
                            </div>
                        </div>
                        <div class="career__list__item__info">
                            <div class="career__list__item__info__txt">
                                <p>
                                    <span>About us:</span><br />
                                    We are developing a smart software that helps people investing into cryptocurrencies without the usual risk and without any prior knowledge. Generally
                                    speaking, our software does three different things. It analyses, calculates, acts and learns. It does this for thousands of different trade opportunities per
                                    minute, millions time per day.
                                </p>
                                <p>
                                    <span>About you:</span><br />
                                    Candidate should have excellent verbal, and written, communication skills; have the ability to work independently; and use good time management practices.
                                </p>
                                <p>
                                    <span>Qualifications:</span><br />
                                    - Strong experience retrieving information from blockchains, and wallets;
                                    - Great Java skills; reactive experience is the plus
                                    - Strog Spring 5, and Spring Boot 2, skills
                                </p>
                                <p>
                                    <span>Education:</span><br />
                                    Bachelor's degree or higher in computer science
                                </p>
                            </div>
                            <div class="career__list__item__info__cta">
                                <a href="#" class="btn">apply for this position</a>
                            </div>
                        </div>
                    </div>

                    <div class="career__list__item" id="career__list__item_6">
                        <div class="career__list__item__head" onclick="openCareerBlock('career__list__item_6')">
                            <div class="lih__left">
                                <div class="lih__left__icon">
                                </div>
                                <div class="lih__left__txt">
                                    <p>development</p>
                                    <p>Blockchain Engineer</p>
                                    <p>London, UK</p>
                                </div>
                            </div>
                            <div class="lih__right">
                                <p>London, UK</p>
                                <p><a class="btn" href="javascript:void(0)">Show detail</a></p>
                            </div>
                        </div>
                        <div class="career__list__item__info">
                            <div class="career__list__item__info__txt">
                                <p>
                                    <span>About us:</span><br />
                                    We are developing a smart software that helps people investing into cryptocurrencies without the usual risk and without any prior knowledge. Generally
                                    speaking, our software does three different things. It analyses, calculates, acts and learns. It does this for thousands of different trade opportunities per
                                    minute, millions time per day.
                                </p>
                                <p>
                                    <span>About you:</span><br />
                                    Candidate should have excellent verbal, and written, communication skills; have the ability to work independently; and use good time management practices.
                                </p>
                                <p>
                                    <span>Qualifications:</span><br />
                                    - Strong experience retrieving information from blockchains, and wallets;
                                    - Great Java skills; reactive experience is the plus
                                    - Strog Spring 5, and Spring Boot 2, skills
                                </p>
                                <p>
                                    <span>Education:</span><br />
                                    Bachelor's degree or higher in computer science
                                </p>
                            </div>
                            <div class="career__list__item__info__cta">
                                <a href="#" class="btn">apply for this position</a>
                            </div>
                        </div>
                    </div>

                    <div class="career__list__item" id="career__list__item_7">
                        <div class="career__list__item__head" onclick="openCareerBlock('career__list__item_7')">
                            <div class="lih__left">
                                <div class="lih__left__icon">
                                </div>
                                <div class="lih__left__txt">
                                    <p>design</p>
                                    <p>UX Designer</p>
                                    <p>UK or Remote</p>
                                </div>
                            </div>
                            <div class="lih__right">
                                <p>UK or Remote</p>
                                <p><a class="btn" href="javascript:void(0)">Show detail</a></p>
                            </div>
                        </div>
                        <div class="career__list__item__info">
                            <div class="career__list__item__info__txt">
                                <p>
                                    <span>About us:</span><br />
                                    We are developing a smart software that helps people investing into cryptocurrencies without the usual risk and without any prior knowledge. Generally
                                    speaking, our software does three different things. It analyses, calculates, acts and learns. It does this for thousands of different trade opportunities per
                                    minute, millions time per day.
                                </p>
                                <p>
                                    <span>About you:</span><br />
                                    Candidate should have excellent verbal, and written, communication skills; have the ability to work independently; and use good time management practices.
                                </p>
                                <p>
                                    <span>Qualifications:</span><br />
                                    - Strong experience retrieving information from blockchains, and wallets;
                                    - Great Java skills; reactive experience is the plus
                                    - Strog Spring 5, and Spring Boot 2, skills
                                </p>
                                <p>
                                    <span>Education:</span><br />
                                    Bachelor's degree or higher in computer science
                                </p>
                            </div>
                            <div class="career__list__item__info__cta">
                                <a href="#" class="btn">apply for this position</a>
                            </div>
                        </div>
                    </div>

                    <div class="career__list__item" id="career__list__item_8">
                        <div class="career__list__item__head" onclick="openCareerBlock('career__list__item_8')">
                            <div class="lih__left">
                                <div class="lih__left__icon">
                                </div>
                                <div class="lih__left__txt">
                                    <p>sales department</p>
                                    <p>Growth Marketer</p>
                                    <p>London, UK</p>
                                </div>
                            </div>
                            <div class="lih__right">
                                <p>London, UK</p>
                                <p><a class="btn" href="javascript:void(0)">Show detail</a></p>
                            </div>
                        </div>
                        <div class="career__list__item__info">
                            <div class="career__list__item__info__txt">
                                <p>
                                    <span>About us:</span><br />
                                    We are developing a smart software that helps people investing into cryptocurrencies without the usual risk and without any prior knowledge. Generally
                                    speaking, our software does three different things. It analyses, calculates, acts and learns. It does this for thousands of different trade opportunities per
                                    minute, millions time per day.
                                </p>
                                <p>
                                    <span>About you:</span><br />
                                    Candidate should have excellent verbal, and written, communication skills; have the ability to work independently; and use good time management practices.
                                </p>
                                <p>
                                    <span>Qualifications:</span><br />
                                    - Strong experience retrieving information from blockchains, and wallets;
                                    - Great Java skills; reactive experience is the plus
                                    - Strog Spring 5, and Spring Boot 2, skills
                                </p>
                                <p>
                                    <span>Education:</span><br />
                                    Bachelor's degree or higher in computer science
                                </p>
                            </div>
                            <div class="career__list__item__info__cta">
                                <a href="#" class="btn">apply for this position</a>
                            </div>
                        </div>
                    </div>

                    <div class="career__list__welcome">
                        <p>
                            Didn't find a right position, but still want to be a part of our journey?<br />
                            Please don’t hesitate to contact us! Send us your CV at <a href="mailto:hr@bitbunch.com">hr@bitbunch.com</a> 
                        </p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();