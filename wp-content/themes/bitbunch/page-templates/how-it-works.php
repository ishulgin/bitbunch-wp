<?php /* Template Name: How-it-works */ get_header(); ?>

<div class="how-it-works">
    <div class="how-it-works__how">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="how-it-works__how__title">
                        <h2>How does BitBunch Work?</h2>
                        <p>
                            BitBunch is a smart trading engine that is directly connceted to all major currency exchanges. Through various AI<br />
                            algorithms, we generate secure profits for you.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row how-it-works__how__body">
                <div class="col how-it-works__how__left">
                    <div class="how-it-works__how__left__block-split">   
                        <div class="how-it-works__how__left__block">
                            <div class="how-it-works__how__left__block__icon">
                                <div><img src="<?php echo get_template_directory_uri(); ?>/images/icons/hiw_cpu_icon.png" /></div>
                            </div>
                            <p class="how-it-works__how__left__block__text">
                                The is one smart wallet for every currency. To start, transfer your assets from your wallet to your smart wallet.
                            </p>
                        </div>
                        <div class="how-it-works__how__left__block">
                            <div class="how-it-works__how__left__block__icon">
                                <div><img src="<?php echo get_template_directory_uri(); ?>/images/icons/hif_thunder_icon.png" /></div>
                            </div>
                            <p class="how-it-works__how__left__block__text">
                                Once your smart wallet is funded, you can choose to either store your funds or activate the secure trading engine. 
                            </p>
                        </div>
                    </div>
                    <div class="how-it-works__how__left__block-split">
                        <div class="how-it-works__how__left__block">
                            <div class="how-it-works__how__left__block__icon">
                                <div><img src="<?php echo get_template_directory_uri(); ?>/images/icons/hiw_shield_icon.png" /></div>
                            </div>
                            <p class="how-it-works__how__left__block__text">
                                On top. you can exchange the assets between your smart wallets and withdraw all your funds whenever you like.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col how-it-works__how__right">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/bg/how_does_it_work_new.png" />
                </div>
            </div>
        </div>
    </div>

    <div class="key_trading_wrap">

        <div class="how-it-works__key">
            <div class="how-it-works__key__title"> 
                <div class="container">
                    <h2>Key Features</h2>
                    <p>
                        We offer you fully automated profit accross all your digital assets while<br />guaranteeing the highest standarts of security.
                    </p>
                </div>
            </div>
            <div class="how-it-works__key__body">
                <div class="container">
                    <div class="how-it-works__key__body__box">
                        <div class="how-it-works__key__body__box__item">
                            <div class="icon">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/icons/shield_check_icon.png" />
                            </div>
                            <h4 class="title">Security</h4>
                            <p class="desc">
                                All your digital assets are secured with a
                                AES-256 encryption and stored offline
                                99.99% of the time.
                            </p>
                        </div>
                        <div class="how-it-works__key__body__box__item">
                            <div class="icon">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/icons/money_icon.png" />
                            </div>
                            <h4 class="title">Multi-currency</h4>
                            <p class="desc">
                                You can invest not only in all major
                                cryptocurrencies but also in Dollar,
                                Euro, Pound, and Yen.
                            </p>
                        </div>
                        <div class="how-it-works__key__body__box__item">
                            <div class="icon">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/icons/chart_icon.png" />
                            </div>
                            <h4 class="title">Automated profits</h4>
                            <p class="desc">
                                Profits get generated fully automatically.
                                No prior investment knowledge is required.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="how-it-works__trading__mobile">
            <div class="how-it-works__trading__mobile__title"> 
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <h2>AI trading</h2>
                            <p>
                                All the trades that our algorithms place are 100% unblased, data-driven and
                                predictable. We combine industry-specific know-low with state-of-the-art
                                technology and machine-learning algorithms.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="how-it-works__trading__mobile__desc-title"> 
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <h3>our three algorithms</h3>
                            <p>
                                While in the stock market arbitrage algorithms have
                                been wide-spread, these tools have not yet been
                                adapted to the volatile cryptocurrency space in an
                                easily accessible way.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="how-it-works__trading__mobile__disc"> 
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="info_box blue">
                                <div class="info_box__text">
                                    <p>Natural language processing</p>
                                    <p>
                                        Our NLP algorithm works with "cypto news scrapper" and a social listing technology to gather in a first step relevant market information.
                                    </p>
                                    <p>
                                        It monitoring the thousands of sources like online magazines, social media accounts of relevant crypto users, company news boards, CEO accounts and more.
                                        Then it evaluates how the information detected will affect the value of certain currencies.
                                    </p>
                                </div>
                            </div>
                            <div class="info_box apply_min_height blue">
                                <div class="info_box__text">
                                    <p>Historical and live data</p>
                                    <p>
                                        The algorithm analyses historical data since 2014 and real-time
                                        data from all major cryptocurrency exchange through
                                        sophisticated APIs. It detects patterns, completes hundreds of
                                        predictions every day without a trade, and it's goal is to spot
                                        live movements which mirrors historical changes and predict the
                                        outcome.
                                    </p>
                                    <p>
                                        It saves the result of these predictions as learnings for further
                                        self-optimisation development.
                                    </p>
                                    <div class="info_box__text__orb left">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/icons/blue_round.png" />
                                    </div>
                                </div>
                            </div>
                            <div class="info_box orange">
                                <div class="info_box__text">
                                    <p>Machine Learning Trading Algorithm</p>
                                    <p>The algorithm combines the outcomes from both other algorithms.</p>
                                    <p>
                                        It can correctly assess the significance of these inputs and how
                                        they affect each other. Then it executes a profitable trade on
                                        behalf of the user.
                                    </p>
                                    <div class="info_box__text__orb">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/icons/orange_round.png" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="how-it-works__trading">
            <div class="how-it-works__trading__title"> 
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <h2>AI trading</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="how-it-works__trading__disc">
                <div class="container">
                    <div class="row how-it-works__trading__disc__row">
                        <div class="col first how-it-works__trading__disc__left">
                            <p>
                                All the trades that our algorithms place are 100% unblased, data-driven and
                                predictable. We combine industry-specific know-low with state-of-the-art
                                technology and machine-learning algorithms.
                            </p>
                            <p>our three algorithms</p>
                            <p>
                                While in the stock market arbitrage algorithms have</br>
                                been wide-spread, these tools have not yet been</br>
                                adapted to the volatile cryptocurrency space in an</br>
                                easily accessible way.
                            </p>
                        </div>
                        <div class="col middle"></div>
                        <div class="col last how-it-works__trading__disc__right">
                            <div class="info_box orange">
                                <div class="info_box__text">
                                    <div class="info_box__text__orb">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/icons/orange_round.png" />
                                    </div>
                                    <p>Machine Learning Trading Algorithm</p>
                                    <p>The algorithm combines the outcomes from both other algorithms.</p>
                                    <p>
                                        It can correctly assess the significance of these inputs and how
                                        they affect each other. Then it executes a profitable trade on
                                        behalf of the user.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row how-it-works__trading__disc__row">
                        <div class="col first">
                            <div class="info_box apply_min_height blue">
                                <div class="info_box__text left">
                                    <div class="info_box__text__orb left">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/icons/blue_round.png" />
                                    </div>
                                    <p>Historical and live data</p>
                                    <p>
                                        The algorithm analyses historical data since 2014 and real-time
                                        data from all major cryptocurrency exchange through
                                        sophisticated APIs. It detects patterns, completes hundreds of
                                        predictions every day without a trade, and it's goal is to spot
                                        live movements which mirrors historical changes and predict the
                                        outcome.
                                    </p>
                                    <p>
                                        It saves the result of these predictions as learnings for further
                                        self-optimisation development.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col middle"></div>
                        <div class="col last">
                            <div class="info_box apply_min_height blue">
                                <div class="info_box__text right">
                                    <div class="info_box__text__orb right">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/icons/blue_round.png" />
                                    </div>
                                    <p>Natural language processing</p>
                                    <p>
                                        Our NLP algorithm works with "cypto news scrapper" and a social listing technology to gather in a first step relevant market information.
                                    </p>
                                    <p>
                                        It monitoring the thousands of sources like online magazines, social media accounts of relevant crypto users, company news boards, CEO accounts and more.
                                        Then it evaluates how the information detected will affect the value of certain currencies.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="how-it-works__profit">
        <div class="how-it-works__profit__title">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h2>How much i can earn with BitBunch?</h2>
                        <p>
                            Market conditions will determine how much you will make. Use the calculator below to get a sense of your<br/>
                            potential profit based on past data in our system.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="how-it-works__profit__body">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="profit_calculator">
                            <div class="profit_calculator__left">
                                <div class="profit_calculator__left__curr">
                                    <p class="profit_calculator__left__curr__title">Currency</p>
                                    <div class="profit_calculator__left__curr__toggles">
                                        <div class="btn-group" data-toggle="buttons">
                                            <div class="btn-group-toggle">
                                                <label class="btn btn-outline-primary active">
                                                    <input type="radio" name="rg-earn-calc-currency" id="fc-earn-calc-btc" checked>
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/icons/btc.svg" alt="BTC">
                                                    BTC
                                                </label>
                                            </div>
                                            <div class="btn-group-toggle">
                                                <label class="btn btn-outline-primary">
                                                    <input type="radio" name="rg-earn-calc-currency" id="fc-earn-calc-usd">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/icons/usd.svg" alt="USD">
                                                    USD
                                                </label>
                                            </div>
                                            <div class="btn-group-toggle">
                                                <label class="btn btn-outline-primary">
                                                    <input type="radio" name="rg-earn-calc-currency" id="fc-earn-calc-eur">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/icons/eur.svg" alt="EUR">
                                                    EUR
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="profit_calculator__left__curr__sliders">
                                        <div class="profit_calculator__left__curr__sliders__slider">
                                            <div class="slider_headline">
                                                <p>Investment amount</p>
                                                <p><span id="investment_amount_slider_value">27</span> <span>BTC</span></p>
                                            </div>
                                            <div class="slider_body">
                                                <div id="investment_amount_slider"></div>
                                            </div>
                                            <div class="slider_bottom">
                                                <p>0 BTC</p>
                                                <p>100 BTC</p>
                                            </div>
                                        </div>
                                        <div class="profit_calculator__left__curr__sliders__slider">
                                            <div class="slider_headline">
                                                <p>Investment period</p>
                                                <p><span id="investment_period_slider_value">12</span> <span>Months</span></p>
                                            </div>
                                            <div class="slider_body">
                                                <div id="investment_period_slider"></div>
                                            </div>
                                            <div class="slider_bottom">
                                                <p>1 Month</p>
                                                <p>24 Months</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="profit_calculator__left__curr__cta">
                                        <a href="#" class="btn btn-lg">start investing today</a>
                                    </div>
                                </div>
                            </div>
                            <div class="profit_calculator__right">
                                <div class="profit_calculator__right__stats-item active">
                                    <p>Your Profit</p>
                                    <p>12 BTC</p>
                                </div>
                                <div class="profit_calculator__right__stats-item">
                                    <p>Your Commission</p>
                                    <p>2.50%</p>
                                </div>
                                <div class="profit_calculator__right__stats-item">
                                    <p>Investment amount</p>
                                    <p>27 BTC</p>
                                </div>
                                <div class="profit_calculator__right__stats-item">
                                    <p>Investment period</p>
                                    <p>12 Months</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="how-it-works__comissions">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="how-it-works__comissions__title">
                        <h2>Commission</h2>
                        <p>
                            Depending on your investment, the</br>
                            commission will change.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row how-it-works__comissions__body">
                <div class="col how-it-works__comissions__left">
                    <p>
                        We have designed a system that generates income for our users by
                        executing the most profitable trades on a daily basis using well known
                        and reliable algorithms.
                    </p>
                    <p>
                        We provide AI-driven software that, very simply, takes advantage of
                        the volatility in the cryptocurrency markets. 
                    </p>
                </div>
                <div class="col how-it-works__comissions__right">
                    <div class="comissions_table">
                        <div class="comissions_table__row">
                            <div class="comissions_table__row__col left">
                                <div class="inside">
                                    <div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/percentage_icon.png" /></div>
                                    <div class="text">
                                        <p>Investment</p>
                                        <p>Up to 5K Euro</p>
                                    </div>
                                </div>
                            </div>
                            <div class="comissions_table__row__col right">
                                <div class="inside">
                                    <p>Commission</p>
                                    <p>30%</p>
                                </div>
                            </div>
                        </div>
                        <div class="comissions_table__row">
                            <div class="comissions_table__row__col left">
                                <div class="inside">
                                    <div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/percentage_icon.png" /></div>
                                    <div class="text">
                                        <p>Investment</p>
                                        <p>5K - 25K Euro</p>
                                    </div>
                                </div>
                            </div>
                            <div class="comissions_table__row__col right">
                                <div class="inside">
                                    <p>Commission</p>
                                    <p>20%</p>
                                </div>
                            </div>
                        </div>
                        <div class="comissions_table__row">
                            <div class="comissions_table__row__col left">
                                <div class="inside">
                                    <div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/percentage_icon.png" /></div>
                                    <div class="text">
                                        <p>Investment</p>
                                        <p>25K - 100K Euro</p>
                                    </div>
                                </div>
                            </div>
                            <div class="comissions_table__row__col right">
                                <div class="inside">
                                    <p>Commission</p>
                                    <p>10%</p>
                                </div>
                            </div>
                        </div>
                        <div class="comissions_table__row">
                            <div class="comissions_table__row__col left">
                                <div class="inside">
                                    <div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/percentage_icon.png" /></div>
                                    <div class="text">
                                        <p>Investment</p>
                                        <p>100K - 500K Euro</p>
                                    </div>
                                </div>
                            </div>
                            <div class="comissions_table__row__col right">
                                <div class="inside">
                                    <p>Commission</p>
                                    <p>5%</p>
                                </div>
                            </div>
                        </div>
                        <div class="comissions_table__row">
                            <div class="comissions_table__row__col left">
                                <div class="inside">
                                    <div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/percentage_icon.png" /></div>
                                    <div class="text">
                                        <p>Investment</p>
                                        <p>500K Euro or more</p>
                                    </div>
                                </div>
                            </div>
                            <div class="comissions_table__row__col right">
                                <div class="inside">
                                    <p>Commission</p>
                                    <p>3%</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="comissions_table_cta">
                        <a href="#" class="btn">start investing today</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();