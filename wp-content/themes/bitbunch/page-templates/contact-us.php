<?php /* Template Name: Contact-us */ get_header(); ?>

<div class="contact-us">
    <div class="contact-us__office">
        <div class="contact-us__office__mobile-form">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <form class="contact_us_form">
                            <div class="contact_us_form__el">
                                <input type="text" class="form-control" placeholder="Your email" />
                            </div>
                            <div class="contact_us_form__el">
                                <input type="text" class="form-control" placeholder="Your name" />
                            </div>
                            <div class="contact_us_form__el">
                                <textarea class="form-control textarea" placeholder="Your message"></textarea>
                            </div>
                            <div class="contact_us_form__el">
                                <button class="btn" onclick="return false;">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="contact-us__office__title">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h2>Our offices</h2>
                        <p>
                            Our doors are always open for you!
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="contact-us__office__body">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="contact-us__map">
                            <div class="contact-us__map__left">
                                <div>
                                    <img class='pin' src="<?php echo get_template_directory_uri(); ?>/images/bg/contact_us_map_pin.png" />
                                    <img class="map" src="<?php echo get_template_directory_uri(); ?>/images/bg/contact_us_map.png" />
                                    <img class="map_mobile" src="<?php echo get_template_directory_uri(); ?>/images/bg/contact_us_map_mobile.png" />
                                </div>
                            </div>
                            <div class="contact-us__map__right">
                                <div>
                                    <p>our address</p>
                                    <p>30 St Mary Axe,<br />London, EC3A 8EP, UK</p>
                                </div>
                                <div>
                                    <p>phone</p>
                                    <p><a href="tel:+447911257986">+44 7911 257986</a></p>
                                </div>
                                <div>
                                    <p>email</p>
                                    <p><a href="mailto:contact@@bitbunch.com">contact@@bitbunch.com</a></p>
                                </div>
                                <div>
                                    <p>company</p>
                                    <p>Bitbunch Ltf,<br />CRN: 06488522</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();