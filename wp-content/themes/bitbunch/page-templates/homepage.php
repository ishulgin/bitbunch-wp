<?php /* Template Name: Homepage */ get_header(); ?>
    <div class="homepage">

        <div class="homepage__offer">
            <div class="homepage__offer__head">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <h2>What we are offering</h2>
                            <p>
                                BitBunch is the best place to securely store all your digital assets
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="homepage__offer__body">
                <div class="homepage__offer__body__img">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/home/offer_image.png" />
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col homepage__offer__body__text">
                            <div class="block">
                                <p>
                                  Automated trading
                                </p>
                                <p>
                                  BitBunch is a cryptocurrency wallet that uses artificial
                                  intelligence to increase the amount of your coins. The
                                  moment you send your assets to BitBunch, enterprise-level
                                  trading algorithms will start generating secure and steady
                                  profits for you. 
                                </p>
                            </div>
                            <div class="block">
                                <p>
                                  No obligations
                                </p>
                                <p>
                                  There are no upfront costs, minimum investments or
                                  obligations. You can try out BitBunch at your very own
                                  terms and will always have 100% control over all your
                                  digital assets.
                                </p>
                            </div>
                            <div class="cta">
                                <a class="btn" href="#">Try free demo</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="homepage__key">
            <div class="homepage__key__title"> 
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <h2>Key Features</h2>
                            <p>
                                We offer you fully automated profit accross all your digital assets while<br />guaranteeing the highest standarts of security.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="homepage__key__body">
                <div class="container">
                    <div class="homepage__key__body__box">
                        <div class="homepage__key__body__box__item">
                            <div class="icon">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/icons/shield_check_icon.png" />
                            </div>
                            <h4 class="title">Licensed</h4>
                            <p class="desc">
                                BitBunch is licensed and registered in
                                Estonia under the license number
                                1840124.
                            </p>
                        </div>
                        <div class="homepage__key__body__box__item">
                            <div class="icon">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/icons/money_icon.png" />
                            </div>
                            <h4 class="title">Transparent</h4>
                            <p class="desc">
                                Our order books are getting updated
                                hourly and you have full access to
                                them. 
                            </p>
                        </div>
                        <div class="homepage__key__body__box__item">
                            <div class="icon">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/icons/chart_icon.png" />
                            </div>
                            <h4 class="title">Insured</h4>
                            <p class="desc">
                                All your assets are insured against
                                hackers and all other types of
                                malicious attacks.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="how_team_wrap">
            <div class="homepage__how">
                <div class="homepage__how__title"> 
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <h2>How it works</h2>
                                <p>
                                    BitBunch automatically exploits certain market movements in order to increase<br />the amount of coins that you are currently holding. 
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="homepage__how__body">
                    <div class="how_cards">
                        <div class="how_cards__row">
                            <div class="how_cards__card">
                                <div class="how_cards__card__left color_yellow">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/home/how/1.png" />
                                </div>
                                <div class="how_cards__card__right">
                                    <p>
                                        <span>0.004826</span><br />
                                        BTC/ZEC
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="how_cards__row">
                            <div class="how_cards__card">
                                <div class="how_cards__card__left color_green">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/home/how/2.png" />
                                </div>
                                <div class="how_cards__card__right">
                                    <p>
                                        <span>0.001049</span><br />
                                        BTC/NEO
                                    </p>
                                </div>
                            </div>
                            <div class="how_cards__card">
                                <div class="how_cards__card__left color_black">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/home/how/3.png" />
                                </div>
                                <div class="how_cards__card__right">
                                    <p>
                                        <span>0.001049</span><br />
                                        BTC/NEO
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="how_cards__row">
                            <div class="how_cards__card">
                                <div class="how_cards__card__left color_light_aqua">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/home/how/4.png" />
                                </div>
                                <div class="how_cards__card__right">
                                    <p>
                                        <span>0.021023</span><br />
                                        BTC/ETH
                                    </p>
                                </div>
                            </div>
                            <div class="how_cards__card">
                                <div class="how_cards__card__left">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/home/how/5.png" />
                                </div>
                                <div class="how_cards__card__right">
                                    <p>
                                        <span>0.005904</span><br />
                                        BTC/LTC
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="how_cards__row">
                            <div class="how_cards__card">
                                <div class="how_cards__card__left color_light_blue">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/home/how/6.png" />
                                </div>
                                <div class="how_cards__card__right">
                                    <p>
                                        <span>0.009857</span><br />
                                        BTC/DASH
                                    </p>
                                </div>
                            </div>
                            <div class="how_cards__card">
                                <div class="how_cards__card__left color_blue">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/home/how/7.png" />
                                </div>
                                <div class="how_cards__card__right">
                                    <p>
                                        <span>0.009857</span><br />
                                        BTC/DASH
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="home_how">
                                    <div class="home_how__left">
                                        <p class="title">Buy low. Sell high.</p>
                                        <div class="desc">
                                            <p>
                                                BitBunch uses different NLP algorithms, AI algorithms and LSTM
                                                neural network models to predict short-term price changes in
                                                cryptocurrencies.
                                            </p>
                                            <p>
                                                When we spot a coin with bullish price patterns, we exchanges your
                                                currency for the bullish currency and later buy them back for a profit.
                                            </p>
                                            <p>
                                                Our machine learning trading engine automatically improves after
                                                every trade.
                                            </p>
                                        </div>
                                        <div class="cta">
                                            <a href="#" class="btn">learn more</a>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="homepage__team">
                <div class="homepage__team__title"> 
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <h2>Our team</h2>
                                <p>
                                    We combine experts from asset management, financial technology, cryptocurrency trading, blockchain<br />
                                    development, artificial intelligence and neural network processing.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="homepage__team__body"> 
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="home_team_video">
                                    <div class="video_cover">
                                      <img class="video_cover__desktop-img" src="<?php echo get_template_directory_uri(); ?>/images/bg/homepage_video_placeholder_big.png" />
                                    </div>
                                </div>
                                <div class="home_team_people">
                                    <div class="people_card">
                                        <div class="people_card__inside">
                                            <p class="people_card__title">James Grade</p>
                                            <p class="people_card__role">CEO</p>
                                            <div class="people_card__img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/home/team/team_1.png" />
                                            </div>
                                            <p class="people_card__desc">
                                                I believe that there is an immense potential in the whole current cryptocurrency market
                                            </p>
                                            <div class="people_card__soc">
                                                <ul>
                                                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/twitter_icon.png" /></a></li>
                                                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/linkedin_icon.png" /></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="people_card">
                                        <div class="people_card__inside">
                                            <p class="people_card__title">Julian Sydney</p>
                                            <p class="people_card__role">CMO</p>
                                            <div class="people_card__img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/home/team/team_2.png" />
                                            </div>
                                            <p class="people_card__desc">
                                                I believe that there is an immense potential in the whole current cryptocurrency market
                                            </p>
                                            <div class="people_card__soc">
                                                <ul>
                                                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/linkedin_icon.png" /></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="people_card">
                                        <div class="people_card__inside">
                                            <p class="people_card__title">Noel Jacobson</p>
                                            <p class="people_card__role">CTO</p>
                                            <div class="people_card__img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/home/team/team_3.png" />
                                            </div>
                                            <p class="people_card__desc">
                                                I believe that there is an immense potential in the whole current cryptocurrency market
                                            </p>
                                            <div class="people_card__soc">
                                                <ul>
                                                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/twitter_icon.png" /></a></li>
                                                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/linkedin_icon.png" /></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="people_card">
                                        <div class="people_card__inside">
                                            <p class="people_card__title">Denise Payne</p>
                                            <p class="people_card__role">CFO</p>
                                            <div class="people_card__img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/home/team/team_4.png" />
                                            </div>
                                            <p class="people_card__desc">
                                                I believe that there is an immense potential in the whole current cryptocurrency market
                                            </p>
                                            <div class="people_card__soc">
                                                <ul>
                                                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/twitter_icon.png" /></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="people_card">
                                        <div class="people_card__inside">
                                            <p class="people_card__title">Adella Evelyn</p>
                                            <p class="people_card__role">PR Manager</p>
                                            <div class="people_card__img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/home/team/team_5.png" />
                                            </div>
                                            <p class="people_card__desc">
                                                I believe that there is an immense potential in the whole current cryptocurrency market
                                            </p>
                                            <div class="people_card__soc">
                                                <ul>
                                                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/linkedin_icon.png" /></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="people_card">
                                        <div class="people_card__inside">
                                            <p class="people_card__title">Jerrie Kay</p>
                                            <p class="people_card__role">Head of Development</p>
                                            <div class="people_card__img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/home/team/team_6.png" />
                                            </div>
                                            <p class="people_card__desc">
                                                I believe that there is an immense potential in the whole current cryptocurrency market
                                            </p>
                                            <div class="people_card__soc">
                                                <ul>
                                                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/twitter_icon.png" /></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="people_card">
                                        <div class="people_card__inside">
                                            <p class="people_card__title">Allie William</p>
                                            <p class="people_card__role">Head of Customer Care</p>
                                            <div class="people_card__img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/home/team/team_7.png" />
                                            </div>
                                            <p class="people_card__desc">
                                                I believe that there is an immense potential in the whole current cryptocurrency market
                                            </p>
                                            <div class="people_card__soc">
                                                <ul>
                                                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/twitter_icon.png" /></a></li>
                                                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/linkedin_icon.png" /></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="people_card">
                                        <div class="people_card__inside">
                                            <p class="people_card__title">Jerrod Sargent</p>
                                            <p class="people_card__role">Sr. Marketing Manager</p>
                                            <div class="people_card__img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/home/team/team_8.png" />
                                            </div>
                                            <p class="people_card__desc">
                                                I believe that there is an immense potential in the whole current cryptocurrency market
                                            </p>
                                            <div class="people_card__soc">
                                                <ul>
                                                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/twitter_icon.png" /></a></li>
                                                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/linkedin_icon.png" /></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="homepage__stats">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="homepage__stats__title">
                            <p>The numbers speak for themselves</p>
                            <p>Bitbunch in stats</p>
                        </div>
                        <div class="homepage__stats__body">
                            <div class="stat_item">
                                <div class="stat_item__inside">
                                    <div class="stat_item__inside__icon">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/icons/money_icon.png" />
                                    </div>
                                    <p class="stat_item__inside__price"><span>$</span>2,856,780</p>
                                    <p class="stat_item__inside__desc">Invested funds</p>
                                </div>
                            </div>
                            <div class="stat_item">
                                <div class="stat_item__inside">
                                    <div class="stat_item__inside__icon">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/icons/dish_icon.png" />
                                    </div>
                                    <p class="stat_item__inside__price">786 521</p>
                                    <p class="stat_item__inside__desc">profitable trades</p>
                                </div>
                            </div>
                            <div class="stat_item">
                                <div class="stat_item__inside">
                                    <div class="stat_item__inside__icon">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/icons/people_icon.png" />
                                    </div>
                                    <p class="stat_item__inside__price">12 478</p>
                                    <p class="stat_item__inside__desc">Active users</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="homepage__trust">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="homepage__trust__title">
                            <h2>Trusted by industry experts </h2>
                            <p>Discover what people are saying about us.</p>
                        </div>
                        <div class="homepage__trust__body">
                            <div class="trust_items">
                                <div class="trust_items__row">
                                    <div class="trust_items__row__column">
                                        <div class="trust_card">
                                            <div class="trust_card__inside">
                                                <div class="trust_card__inside__img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/home/trust/trust_1.png" />
                                                    <a href="#" class="btn">Interview</a>
                                                </div>
                                                <p class="trust_card__inside__title">Blockchain 3.0?</p>
                                                <p class="trust_card__inside__date">20.09.2018</p>
                                            </div>
                                        </div>
                                        <div class="trust_card">
                                            <div class="trust_card__inside">
                                                <div class="trust_card__inside__img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/home/trust/trust_2.png" />
                                                    <a href="#" class="btn l_orange">Partners</a>
                                                </div>
                                                <p class="trust_card__inside__title">Ripple Grapples</p>
                                                <p class="trust_card__inside__date">15.09.2018</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="trust_items__row">
                                    <div class="trust_items__row__column">
                                        <div class="trust_card">
                                            <div class="trust_card__inside">
                                                <div class="trust_card__inside__img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/home/trust/trust_3.png" />
                                                    <a href="#" class="btn blue">Interview</a>
                                                </div>
                                                <p class="trust_card__inside__title">Meet 100+ Top Industry Experts</p>
                                                <p class="trust_card__inside__desc">
                                                    Join us at the International Conference, Oct 28-29, 2018 in Zurich,
                                                    Switzerland. Register now and save up to 38% with early rates! 100+
                                                    Global Speakers. Networking Opportunities. Extended 3 Track Program.
                                                </p>
                                                <p class="trust_card__inside__date">28.09.2018</p>
                                            </div>
                                        </div>
                                        <div class="trust_card">
                                            <div class="trust_card__inside">
                                                <div class="trust_card__inside__img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/home/trust/trust_4.png" />
                                                    <a href="#" class="btn l_orange">Regulation</a>
                                                </div>
                                                <p class="trust_card__inside__title">Regulating the new economy</p>
                                                <p class="trust_card__inside__date">20.09.2018</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="trust_items__row">
                                    <div class="trust_items__row__column">
                                        <div class="trust_card">
                                            <div class="trust_card__inside">
                                                <div class="trust_card__inside__img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/home/trust/trust_5.png" />
                                                    <a href="#" class="btn l_orange">About us</a>
                                                </div>
                                                <p class="trust_card__inside__title">G20 Crypto Regulations</p>
                                                <p class="trust_card__inside__date">21.09.2018</p>
                                            </div>
                                        </div>
                                        <div class="trust_card">
                                            <div class="trust_card__inside">
                                                <div class="trust_card__inside__img">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/images/home/trust/trust_6.png" />
                                                    <a href="#" class="btn">Interview</a>
                                                </div>
                                                <p class="trust_card__inside__title">Who affect BTC price?</p>
                                                <p class="trust_card__inside__date">13.09.2018</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="trust_cta">
                                <a href="#" class="btn">More Articles</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();