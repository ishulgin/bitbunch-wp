<?php /* Template Name: Pressroom */ get_header(); ?>

<div class="pressroom">
    <div class="pressroom__release">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="pressroom__release__title">
                        <h2>New Releases</h2>
                        <p>
                            Stay update with what's happening at Bitbunch
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="pressroom__release__pcards">
                        <div class="card-exp">
                            <div class="card-exp__inside">
                                <a class="btn card-exp__inside__cta" href="#">About us</a>
                                <img src="<?php echo get_template_directory_uri(); ?>/images/release/release_1.png" class="card-exp__inside__img" />
                                <div class="card-exp__inside__back">
                                    <div class="info-exp-main">
                                        <p>05.10.2018</p>
                                        <h3>Why automation is better?</h3>
                                        <a class="btn" href="#">About us</a>
                                    </div>
                                    <div class="info-exp-sec">
                                        <div class="info-exp-sec__inside">
                                            <p>
                                                The stock of Company X is trading at $20 on the New York Stock Exchange (NYSE) while,
                                                at the same moment, it is trading for $20.05 on the London Stock Exchange (LSE). A trader can
                                                buy the stock on the NYSE and immediately sell the same shares on the LSE...
                                            </p>
                                            <a href="#">Read more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-exp">
                            <div class="card-exp__inside">
                                <a class="btn card-exp__inside__cta" href="#">About us</a>
                                <img src="<?php echo get_template_directory_uri(); ?>/images/release/release_2.png" class="card-exp__inside__img" />
                                <div class="card-exp__inside__back">
                                    <div class="info-exp-main">
                                        <p>05.10.2018</p>
                                        <h3>Why automation is better?</h3>
                                        <a class="btn" href="#">About us</a>
                                    </div>
                                    <div class="info-exp-sec">
                                        <div class="info-exp-sec__inside">
                                            <p>
                                                The stock of Company X is trading at $20 on the New York Stock Exchange (NYSE) while,
                                                at the same moment, it is trading for $20.05 on the London Stock Exchange (LSE). A trader can
                                                buy the stock on the NYSE and immediately sell the same shares on the LSE...
                                            </p>
                                            <a href="#">Read more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-exp">
                            <div class="card-exp__inside">
                                <a class="btn card-exp__inside__cta" href="#">About us</a>
                                <img src="<?php echo get_template_directory_uri(); ?>/images/release/release_3.png" class="card-exp__inside__img" />
                                <div class="card-exp__inside__back">
                                    <div class="info-exp-main">
                                        <p>05.10.2018</p>
                                        <h3>Why automation is better?</h3>
                                        <a class="btn" href="#">About us</a>
                                    </div>
                                    <div class="info-exp-sec">
                                        <div class="info-exp-sec__inside">
                                            <p>
                                                The stock of Company X is trading at $20 on the New York Stock Exchange (NYSE) while,
                                                at the same moment, it is trading for $20.05 on the London Stock Exchange (LSE). A trader can
                                                buy the stock on the NYSE and immediately sell the same shares on the LSE...
                                            </p>
                                            <a href="#">Read more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-exp">
                            <div class="card-exp__inside">
                                <a class="btn card-exp__inside__cta" href="#">About us</a>
                                <img src="<?php echo get_template_directory_uri(); ?>/images/release/release_4.png" class="card-exp__inside__img" />
                                <div class="card-exp__inside__back">
                                    <div class="info-exp-main">
                                        <p>05.10.2018</p>
                                        <h3>Why automation is better?</h3>
                                        <a class="btn" href="#">About us</a>
                                    </div>
                                    <div class="info-exp-sec">
                                        <div class="info-exp-sec__inside">
                                            <p>
                                                The stock of Company X is trading at $20 on the New York Stock Exchange (NYSE) while,
                                                at the same moment, it is trading for $20.05 on the London Stock Exchange (LSE). A trader can
                                                buy the stock on the NYSE and immediately sell the same shares on the LSE...
                                            </p>
                                            <a href="#">Read more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-exp">
                            <div class="card-exp__inside">
                                <a class="btn card-exp__inside__cta" href="#">About us</a>
                                <img src="<?php echo get_template_directory_uri(); ?>/images/release/release_5.png" class="card-exp__inside__img" />
                                <div class="card-exp__inside__back">
                                    <div class="info-exp-main">
                                        <p>05.10.2018</p>
                                        <h3>Why automation is better?</h3>
                                        <a class="btn" href="#">About us</a>
                                    </div>
                                    <div class="info-exp-sec">
                                        <div class="info-exp-sec__inside">
                                            <p>
                                                The stock of Company X is trading at $20 on the New York Stock Exchange (NYSE) while,
                                                at the same moment, it is trading for $20.05 on the London Stock Exchange (LSE). A trader can
                                                buy the stock on the NYSE and immediately sell the same shares on the LSE...
                                            </p>
                                            <a href="#">Read more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-exp">
                            <div class="card-exp__inside">
                                <a class="btn card-exp__inside__cta" href="#">About us</a>
                                <img src="<?php echo get_template_directory_uri(); ?>/images/release/release_5.png" class="card-exp__inside__img" />
                                <div class="card-exp__inside__back">
                                    <div class="info-exp-main">
                                        <p>05.10.2018</p>
                                        <h3>Why automation is better?</h3>
                                        <a class="btn" href="#">About us</a>
                                    </div>
                                    <div class="info-exp-sec">
                                        <div class="info-exp-sec__inside">
                                            <p>
                                                The stock of Company X is trading at $20 on the New York Stock Exchange (NYSE) while,
                                                at the same moment, it is trading for $20.05 on the London Stock Exchange (LSE). A trader can
                                                buy the stock on the NYSE and immediately sell the same shares on the LSE...
                                            </p>
                                            <a href="#">Read more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pressroom__brands">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="pressroom__brands__title">
                        <h2>Our Brand Standards</h2>
                        <p>
                            Be inspired by our vision and brand standards. Use our logo formats below.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="pressroom__brands__body">
                        <div class="pressroom__brands__body__left">
                            <div class="pressroom__brands__body__left__inside">
                                <div class="headline">
                                    <p>Our Logo</p>
                                    <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/cloud-download.svg" /></a>
                                </div>
                                <div class="guide">
                                    <div class="guide__left">
                                        <div class="guide__left__inside">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/release/rel_logo_big.png" />
                                        </div>
                                    </div>
                                    <div class="guide__right">
                                        <div class="guide__right__inside">
                                            <div class="t_white">
                                                <div class="t_white__inside">
                                                    <div class="t_white__inside__img-flex">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/images/release/rel_logo_pic.png" />
                                                        <img src="<?php echo get_template_directory_uri(); ?>/images/release/rel_logo_text.png" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="t_black">
                                                <div class="t_black__inside">
                                                    <div class="t_black__inside__img-flex">
                                                        <img src="<?php echo get_template_directory_uri(); ?>/images/release/rel_logo_pic_revert.png" />
                                                        <img src="<?php echo get_template_directory_uri(); ?>/images/release/rel_logo_text_revert.png" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <p class="desc">
                                    This is our logo, the one that should always be used. There are three different versions to ensure optimum legibility.
                                </p>
                            </div>
                        </div>
                        <div class="pressroom__brands__body__right">
                            <div class="pressroom__brands__body__right__inside">
                                <div class="headline">
                                    <p>Brand Guidelines</p>
                                    <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/documents.svg" /></a>
                                </div>
                                <div class="guide">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/release/guide.png" />
                                </div>
                                <p class="desc">
                                    This is our logo, the one that should always be used. There are three different versions to ensure optimum legibility.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();